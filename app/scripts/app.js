'use strict';

/**
 * @ngdoc overview
 * @name networkSandboxApp
 * @description
 * # networkSandboxApp
 *
 * Main module of the application.
 */
(function(){
var app = angular.module('networkSandboxApp', [
    'ngAnimate',
    'ngResource',
    'ngRoute',
    'ui.bootstrap', 
    'NetworkServices',
    'server', 
    'panels'
  ]);

  app.val = 0;

    
})();

 'use strict';

(function(){

	var app = angular.module('FSMService', []);
	app.factory('fsm', [ function(){
		//var self = this;
		//Holds all states created
		// key: name, value: CState
		var _States = new CMap();

		//  Finite State Machine
		var CFiniteStateMachine = function(cEntity){
		    var owner = cEntity;
		    var _CurrentState = new CState();
		    //var self = this;
		    var _Enter = function(oMessage){
		        _CurrentState.Enter(owner, oMessage);
		    };
		    var _Exit = function(oMessage){
		        _CurrentState.Exit(owner, oMessage);
		    };
		    var _Execute = function(oMessage){
		        _CurrentState.Execute(owner, oMessage);
		    };
		    
		    //Control Methods
		    this.Transition = function(sName, oMessage){
		        //Call Exit for Current State
		        _Exit(oMessage);
		        
		        //Get New State to Transition Too
		        _CurrentState = _States.get(sName);
		        
		        //Call New States Setup and Exectue Methods
		        _Enter(oMessage);
		        _Execute(oMessage);
		    };
		    this.SetState = function(sName){
		        _CurrentState = _States.get(sName);
		    };
		    this.Update = function(oMessage){
		        _Execute(oMessage);
		    };
		    this.GetState = function(){
		        return _CurrentState.GetName();
		    };
		};

		// States for FSM
		var CState = function(sName){
		    var _name = sName || 'default';
		    //var self = this;

		    this.GetName = function(){
		        return _name;
		    };
		};
			//Inputs: cEntity, oMessage
		CState.prototype.Enter = function(){
		    //Override this Method
		};
		CState.prototype.Exit = function(){
		    //Override this Method
		};
		CState.prototype.Execute = function(){
		    //Override this Method
		};
		    
		var _fsm = new CFiniteStateMachine(this);

		//Expose Map class to API
		_fsm.Map = CMap;

		//Extend FSM to add API interface
		_fsm.State = {
		        Create: function(sName){
		            var state = new CState(sName);
		            _States.put(sName, state);
		            return state;
		        },
		        Get: function(sName){
		            return _States.get(sName);
		        },
		        Set: function(sName){
		        	_fsm.SetState(sName);
		        }
		    };

		return _fsm;
	}]);

 var CMap = function(linkEntries){
    this.current = undefined;
    this.size = 0;
    this.isLinked = true;

    if(linkEntries === false)
    {
            this.disableLinking();
    }
            
    this.from = function(obj, foreignKeys, linkEntries)
    {
        var map = new Map(linkEntries);

        for(var prop in obj) {
                if(foreignKeys || obj.hasOwnProperty(prop)){
                        map.put(prop, obj[prop]);
                }
        }

        return map;
    };
    
    this.noop = function()
    {
            return this;
    };
    
    this.illegal = function()
    {
            throw new Error('can\'t do this with unlinked maps');
    };
    
    this.reverseIndexTableFrom = function(array, linkEntries)
    {
        var map = new CMap(linkEntries);

        for(var i = 0, len = array.length; i < len; ++i) {
                var	entry = array[i],
                        list = map.get(entry);

                if(list){ list.push(i);}
                else{map.put(entry, [i]);}
        }

        return map;
    };

    this.cross = function(map1, map2, func, thisArg)
    {
        var linkedMap, otherMap;
    
        if(map1.isLinked) {
                linkedMap = map1;
                otherMap = map2;
        }
        else if(map2.isLinked) {
                linkedMap = map2;
                otherMap = map1;
        }
        else {CMap.illegal();}
    
        for(var i = linkedMap.size; i--; linkedMap.next()) {
                var key = linkedMap.key();
                if(otherMap.contains(key)){
                        func.call(thisArg, key, map1.get(key), map2.get(key));
                    }
        }
    
        return thisArg;
    };

    this.uniqueArray = function(array)
    {
            var map = new CMap();
    
            for(var i = 0, len = array.length; i < len; ++i){
                    map.put(array[i]);
                }
    
            return map.listKeys();
    };  

    this._hash = 'object ' + CMap._counter++;                              
};

CMap.prototype.disableLinking = function(){
    this.isLinked = false;
    this.link = Map.noop;
    this.unlink = Map.noop;
    this.disableLinking = Map.noop;
    this.next = Map.illegal;
    this.key = Map.illegal;
    this.value = Map.illegal;
    this.removeAll = Map.illegal;
    this.each = Map.illegal;
    this.flip = Map.illegal;
    this.drop = Map.illegal;
    this.listKeys = Map.illegal;
    this.listValues = Map.illegal;

    return this;
};
CMap._counter = 0;
CMap.prototype.hash = function(value){
	return (value && value._hash) || (typeof(value) + ' ' + String(value));

    // return value instanceof Object ? (value.__hash ||
    //         (value.__hash = 'object ' + (++arguments.callee.current))) :
    //         (typeof value) + ' ' + String(value);
};

CMap.prototype.hash.current = 0;            
CMap.prototype.link = function(entry){
        if(this.size === 0) {
                entry.prev = entry;
                entry.next = entry;
                this.current = entry;
        }
        else {
                entry.prev = this.current.prev;
                entry.prev.next = entry;
                entry.next = this.current;
                this.current.prev = entry;
        }
};

CMap.prototype.unlink = function(entry) {
        if(this.size === 0){
                this.current = undefined;
            }
        else {
                entry.prev.next = entry.next;
                entry.next.prev = entry.prev;
                if(entry === this.current){
                        this.current = entry.next;
                    }
        }
};

CMap.prototype.get = function(key) {
        var entry = this[this.hash(key)];
        return typeof entry === 'undefined' ? undefined : entry.value;
};

CMap.prototype.put = function(key, value) {
        var hash = this.hash(key);

        if(this.hasOwnProperty(hash)){
                this[hash].value = value;
            }
        else {
                var entry = { key : key, value : value };
                this[hash] = entry;

                this.link(entry);
                ++this.size;
        }

        return this;
};

CMap.prototype.remove = function(key) {
        var hash = this.hash(key);

        if(this.hasOwnProperty(hash)) {
                --this.size;
                this.unlink(this[hash]);

                delete this[hash];
        }

        return this;
};

CMap.prototype.removeAll = function() {
        while(this.size){
                this.remove(this.key());
            }

        return this;
};

CMap.prototype.contains = function(key) {
        return this.hasOwnProperty(this.hash(key));
};

CMap.prototype.isUndefined = function(key) {
        var hash = this.hash(key);
        return this.hasOwnProperty(hash) ?
                typeof this[hash] === 'undefined' : false;
};

CMap.prototype.next = function() {
        this.current = this.current.next;
};

CMap.prototype.key = function() {
        return this.current.key;
};

CMap.prototype.value = function() {
        return this.current.value;
};

CMap.prototype.each = function(func, thisArg) {
        if(typeof thisArg === 'undefined'){
                thisArg = this;
            }

        for(var i = this.size; i--; this.next()) {
                var n = func.call(thisArg, this.key(), this.value(), i > 0);
                if(typeof n === 'number'){
                        i += n; // allows to add/remove entries in func
                    }
        }

        return this;
};

CMap.prototype.flip = function(linkEntries) {
        var map = new Map(linkEntries);

        for(var i = this.size; i--; this.next()) {
                var	value = this.value();
                var list = map.get(value);

                if(list){ list.push(this.key());}
                else {map.put(value, [this.key()]);}
        }

        return map;
};

CMap.prototype.drop = function(func, thisArg) {
        if(typeof thisArg === 'undefined'){
                thisArg = this;
            }

        for(var i = this.size; i--; ) {
                if(func.call(thisArg, this.key(), this.value())) {
                        this.remove(this.key());
                        --i;
                }
                else{this.next();}
        }

        return this;
};

CMap.prototype.listValues = function() {
        var list = [];

        for(var i = this.size; i--; this.next()){
                list.push(this.value());
            }

        return list;
};

CMap.prototype.listKeys = function() {
        var list = [];

        for(var i = this.size; i--; this.next()){
                list.push(this.key());
            }

        return list;
};

CMap.prototype.toString = function() {
        var string = '[object Map';
        var self = this;

        function addEntry(key, value, hasNext) {
                string += '    { ' + self.hash(key) + ' : ' + value + ' }' + (hasNext ? ',' : '') + '\n';
        }

        if(this.isLinked && this.size) {
                string += '\n';
                this.each(addEntry);
        }

        string += ']';
        return string;
};

    

})();

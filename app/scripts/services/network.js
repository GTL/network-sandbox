 'use strict';

(function(){
	var socketio = document.createElement('script');
   	socketio.type = 'text/javascript';
   	socketio.src = 'https://cdn.socket.io/socket.io-1.0.0.js';
   	document.body.appendChild(socketio);
   	
	var app = angular.module('NetworkServices', []);
	app.factory('socket', ['$rootScope', function($rootScope){
		
		var ip = '129.119.228.109';
		var port = '8080';
		var socket = null;

		var callbacks = {};

		var api = {
			on: function (eventName, callback) {
				try{
					var id = eventName + Date.now();
					callbacks[id] = function () {  
			        	var args = arguments;
			        	$rootScope.$apply(function () {
			          		callback.apply(socket, args);
			        	});
			      	};
			      socket.on(eventName, callbacks[id] );
			      return id;
				}
				catch(e){
					return null;
				}
				
		    },
		    emit: function (eventName, data, callback) {
		      socket.emit(eventName, data, function () {
		        var args = arguments;
		        $rootScope.$apply(function () {
		          if (callback) {
		            callback.apply(socket, args);
		          }
		        });
		      });
		    },
		    removeListener: function(eventName, id){
		    	try{
		    		socket.removeListener(eventName, callbacks[id]);
		    		callbacks[id] = null;
		    	}
		    	catch(e){
		    		
		    	}
		    	

		    },
			connect: function(IP, PORT){
					ip = IP;
					port = PORT;
		   			console.log('Connecting to ' + ip + ':' + port);
		   			if(socket){
		   				socket.connect(ip + ':' + port);
		   			}
		   			else{
		   				socket = window.io.connect(ip + ':' + port);

		   				api.on('clients', function(msg){
		   				console.log('Client Update');
		   				api.clients.tcp = msg.tcp;
		   				api.clients.udp = msg.udp;
		   				api.clients.browser = msg.browser;
		   			});
		   			}
		   			
		   			
		   	},
		   	close: function(){
		   		console.log('Closed connection with Server');
		   		socket.close();
		   		//socket.destroy();
		   		//socket = null;
		   	},
		   	clients: {
		   		tcp: 0,
		   		udp: 0,
		   		browser: 0
		   	}

		};

		return api;
	}]);
})();

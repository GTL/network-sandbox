 'use strict';

(function(){
	var app = angular.module('packetInspector', ['ui.bootstrap', 'NetworkServices', 'FSMService']);

	app.controller('InspectorCtrl',['socket', 'fsm', '$scope', function (socket, fsm, $scope) {
		var self = this;
		
		fsm.Transition('PacketInspector');
		this.state = fsm.State.Get('PacketInspector');

		//this.messages  = 'Packets will go here \n';
		
		//Adds Message to View
		this.AppendMessage = function(msg){
			this.state.messages = msg + '\n' + this.state.messages;
		};
		

		//Event Listener for WebServer
		this.onMessage = function(msg){
			var value = '';
			var date = new Date();
			var time = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds() + ':' + date.getMilliseconds();
			//Test if packet
			if(msg.message.id){
				value = JSON.stringify(msg.message, undefined, 4);

			}
			else{
				value = String.fromCharCode.apply(null, new Uint8Array(msg.message));
			}
			
			var out = 'Got ' + msg.type + ' message at: ' + time;
			out += '\nMessage: \n' + value + '\n';

			//console.log(out);
			self.AppendMessage(out);

		};

		//Enable/Disable Event Listener from WebServer
		this.Activate = function(){
			//Add event listeners
			if(self.state.active){
				console.log('Adding Packet Inspector Event Listeners');
				self.onMessage.id = socket.on('message',self.onMessage);
				
				if(self.onMessage.id === null){
					$scope.$parent.alerts.push({type: 'danger', msg: 'You Must Connect to Server Before Activating'});
					self.state.active = false;
				}

			}
			//remove event listeners
			else{
				console.log('Removing Packet Inspector Event Listeners');
				socket.removeListener('message', self.onMessage.id);
			}
		};

	 }]);

	app.directive('packetInspector', function(){
		return {
			restrict: 'E',
			templateUrl: 'views/inspector.html',
			controller: 'InspectorCtrl',
			controllerAs: 'inspector'
		};
	});

	
	

})();
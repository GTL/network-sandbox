 'use strict';

(function(){
	var app = angular.module('server', ['ui.bootstrap', 
		'NetworkServices',
		'FSMService',
		'packetInspector',
		'packetTab',
		'interpolationTab']);
	
	app.LoadStates = function(fsm){
		window.requestAnimationFrame = (function(){
		  return  window.requestAnimationFrame       ||
		          window.webkitRequestAnimationFrame ||
		          window.mozRequestAnimationFrame    ||
		          function( callback ){
		            window.setTimeout(callback, 1000 / 60);
		          };
		})();

		//
		//	Packet Inspector State
		//
		var SPacketInspector = fsm.State.Create('PacketInspector');
		SPacketInspector.Enter = function(scope, message){
			scope.msg = 'yo';
			message = undefined;
		};
		SPacketInspector.Execute = function(scope, message){
			scope.msg = 'yo';
			message = undefined;
		};
		SPacketInspector.Exit = function(scope, message){
			scope.msg = 'yo';
			message = undefined;
		};
		SPacketInspector.active = false;
		SPacketInspector.messages = 'Packets will go here \n';

		//
		//	Packet Position State
		//
		var SPacketPosition = fsm.State.Create('PacketPosition');
		SPacketPosition.Enter = function(scope, message){
			scope.msg = 'yo';
			message = undefined;
		};
		SPacketPosition.Execute = function(scope, message){
			scope.msg = 'yo';
			message = undefined;
		};
		SPacketPosition.Exit = function(scope, message){
			scope.msg = 'yo';
			message = undefined;
		};
		SPacketPosition.active = false;
		SPacketPosition.position = {
			x: 0,
			y: 0
		};

		//
		//	Interpolation State
		//
		var SInterpolation = fsm.State.Create('Interpolation');
		SInterpolation.Enter = function(scope, message){
			scope.msg = 'yo';
			message = undefined;
		};
		SInterpolation.Execute = function(scope, message){
			scope.msg = 'yo';
			message = undefined;
		};
		SInterpolation.Exit = function(scope, message){
			scope.msg = 'yo';
			message = undefined;
		};
		SInterpolation.active = false;
		//Map to store all players
		SInterpolation.Players = new fsm.Map();

		//
		//	Dead Reckoning State
		//
		var SDeadReckoning = fsm.State.Create('DeadReckoning');
		SDeadReckoning.Enter = function(scope, message){
			scope.msg = 'yo';
			message = undefined;
		};
		SDeadReckoning.Execute = function(scope, message){
			scope.msg = 'yo';
			message = undefined;
		};
		SDeadReckoning.Exit = function(scope, message){
			scope.msg = 'yo';
			message = undefined;
		};




	};

	app.controller('ServerCtrl',['socket','fsm', '$scope', function (socket, fsm) {

	   		var self = this;
	   		self.isCollapsed = true;
	   		self.ip = 'smu.gametheorylabs.com';
	   		self.port = '8000';
	   		
	   		self.close = function(){
	   			socket.close();
	   		};
	   		self.connect = function(){
	   			socket.connect(self.ip, self.port);
	   			// socket.emit('message', 'Hello from Browser');

	   			// var msgHandler = function(msg){
	   			// 	console.log(msg);
	   			// 	socket.emit('message', 'Hello from Browser Again');
	   			// 	socket.removeListener('message', msgHandler.id);
	   			// }

	   			// msgHandler.id = socket.on('message', msgHandler);
	   		};

	   		app.LoadStates(fsm);

	  }]);
	
	

	app.directive('server', function(){
		return {
			restrict: 'E',
			templateUrl: 'views/server.html',
			controller: 'ServerCtrl',
			controllerAs: 'server'
		};
	});


})();
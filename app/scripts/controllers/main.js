'use strict';

/**
 * @ngdoc function
 * @name networkSandboxApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the networkSandboxApp
 */
angular.module('networkSandboxApp')
  .controller('MainCtrl', function ($scope) {

    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

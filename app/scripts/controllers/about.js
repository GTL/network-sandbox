'use strict';

/**
 * @ngdoc function
 * @name networkSandboxApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the networkSandboxApp
 */
angular.module('networkSandboxApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

 'use strict';

(function(){
	var app = angular.module('panels', ['ui.bootstrap', 'NetworkServices']);


	app.controller('PanelsCtrl',['socket', '$scope', function (socket, $scope) {
	 	this.tab = 1;

	 	//Bind to socket service clients object
	 		//Outputs number of TCP/UDP/Browsers Connected to WebServer
	 	this.clients = socket.clients;

	 	$scope.alerts = [];

		  $scope.addAlert = function() {
		    $scope.alerts.push({msg: 'Another alert!'});
		  };

		  $scope.closeAlert = function(index) {
		    $scope.alerts.splice(index, 1);
		  };

	 	

	 }]);

	app.directive('panels', function(){
		return {
			restrict: 'E',
			templateUrl: 'views/panels.html',
			controller: 'PanelsCtrl',
			controllerAs: 'panel'
		};
	});

})();
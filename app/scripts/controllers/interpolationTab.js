 'use strict';

(function(){
	var app = angular.module('interpolationTab', ['ui.bootstrap', 'NetworkServices', 'FSMService']);

	app.controller('InterpolationTabCtrl',['socket', 'fsm', '$scope', function (socket, fsm, $scope) {
		var self = this;
		
		fsm.Transition('Interpolation');
		this.state = fsm.State.Get('Interpolation');
		this.players = this.state.Players;

		this.canvas = document.getElementById('interpolationTabCanvas');
		this.ctx = this.canvas.getContext('2d');
		
		this.drawRect = function(x, y, color, scale){
			this.ctx.fillStyle = 'rgb(' + color +')';
			this.ctx.fillRect(x, y, 5 * scale, 5 * scale);
		};


		this.Update = function(){
			if(self.state.active && $scope.$parent && ($scope.$parent.panel.tab === 3)){
				//Set Callback
				window.requestAnimationFrame(self.Update);

				var time = Date.now();
				var deadClients = [];

				//Clear Screen
				self.canvas.width = self.canvas.width;

				for(var i = self.players.size; i > 0; i--){
					var player = self.players.value();

					//Only update connected clients
					if( (time - player.time) < 5000 ){
						var dx = player.x - player.currentX;
						var dy = player.y - player.currentY;

						//Update Position
						if(dx > 0){
							player.currentX+=1;
						}
						else if(dx < 0){
							player.currentX-=1;
						}

						if(dy > 0){
							player.currentY+=1;
						}
						else if(dy , 0){
							player.currentY-=1;
						}

						//Draw Player
						self.drawRect(player.currentX, player.currentY, player.id, 2);
					}
					else{
						deadClients.push(player);
					}

					self.players.next();
				}
				
				//Delete Dead Clients
				for(var j = deadClients.length - 1; j >= 0; j--){
					self.players.remove(deadClients[j].id);
				}
			}
		};

		//Event Listener for WebServer
		this.onMessage = function(msg){
			
			//Test packet type
			if(msg.message.id === 2){
				var player = self.players.get(msg.message.playerID);

				if(!player){
					player = {};
					player.id = msg.message.playerID;
					
					//color
					player.r = msg.message.r;
					player.g = msg.message.g;
					player.b = msg.message.b;
					
					//Set current position
					player.currentX = msg.message.x;
					player.currentY = msg.message.y;

					self.players.put(player.id, player);
				}

				//Update Target Position
				player.x = msg.message.x;
				player.y = msg.message.y;

				//Update time
				player.time = Date.now();
			}
		};

		//Enable/Disable Event Listener from WebServer
		this.Activate = function(){
			//this.drawRect(200,200,'black', 10);

			//Add event listeners
			if(self.state.active){
				console.log('Adding Interpolation Tab Event Listeners');
				self.onMessage.id = socket.on('message',self.onMessage);
				if(self.onMessage.id === null){
					$scope.$parent.alerts.push({type: 'danger', msg: 'You Must Connect to Server Before Activating'});
					self.state.active = false;
				}
				else{
					self.Update();
				}

			}
			//remove event listeners
			else{
				console.log('Removing Interpolation Tab Event Listeners');
				socket.removeListener('message', self.onMessage.id);
			}
		};

		if(this.state.active){
			this.Update();
		}

	 }]);

	app.directive('interpolationTab', function(){
		return {
			restrict: 'E',
			templateUrl: 'views/interpolationTab.html',
			controller: 'InterpolationTabCtrl',
			controllerAs: 'interpolationTab'
		};
	});
	

})();

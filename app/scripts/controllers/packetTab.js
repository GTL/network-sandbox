 'use strict';

(function(){
	var app = angular.module('packetTab', ['ui.bootstrap', 'NetworkServices', 'FSMService']);

	app.controller('PacketTabCtrl',['socket', 'fsm', '$scope', function (socket, fsm, $scope) {
		var self = this;
		
		fsm.Transition('PacketPosition');
		this.state = fsm.State.Get('PacketPosition');

		this.canvas = document.getElementById('packetTabCanvas');
		this.ctx = this.canvas.getContext('2d');
		
		this.drawRect = function(x, y, scale){
			this.state.position.x = x;
			this.state.position.y = y;
			this.ctx.fillRect(this.state.position.x, this.state.position.y, 5 * scale, 5 * scale);
		};

		
		//Event Listener for WebServer
		this.onMessage = function(msg){
			
			//Test packet type
			if(msg.message.id === 1){
				self.canvas.width = self.canvas.width;
				self.drawRect(msg.message.x, msg.message.y, 2);

			}

		};

		//Enable/Disable Event Listener from WebServer
		this.Activate = function(){
			//this.drawRect(200,200,'black', 10);

			//Add event listeners
			if(self.state.active){
				console.log('Adding PacketTab Event Listeners');
				self.onMessage.id = socket.on('message',self.onMessage);
				if(self.onMessage.id === null){
					$scope.$parent.alerts.push({type: 'danger', msg: 'You Must Connect to Server Before Activating'});
					self.state.active = false;
				}

			}
			//remove event listeners
			else{
				console.log('Removing PacketTab Event Listeners');
				socket.removeListener('message', self.onMessage.id);
			}
		};

		if(this.state.active){
			this.drawRect(this.state.position.x, this.state.position.y, 'black', 2);
		}

	 }]);

	app.directive('packetTab', function(){
		return {
			restrict: 'E',
			templateUrl: 'views/packetTab.html',
			controller: 'PacketTabCtrl',
			controllerAs: 'packetTab'
		};
	});

	
	

})();
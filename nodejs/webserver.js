var websocketPort = 8000;
var tcpPort       = 6000;
var udpPort       = 5000;

var tools = require('./cmap.js');
//Binary Parser
//var binary  = require('binary');
//var binary  = require('dissolve');

//Packet Types
var POSITION = 0x01;
var POSITIONCOLOR = 0x02;

var PacketParse = function(buff){
	var out =  {};
	var offset = 0;

	//Get Packet ID
	out.id = buff.readUInt8(offset);
	
	//Decode Packets
	if(out.id === POSITION){
		//Padding
		offset += 4;

		out.x  = buff.readFloatLE(offset);
		offset += 4;

		out.y  = buff.readFloatLE(offset);
	}
	else if(out.id === POSITIONCOLOR){
		//Color
		offset += 1;
		out.r = buff.readUInt8(offset);

		offset += 1;
		out.g = buff.readUInt8(offset);

		offset += 1;
		out.b = buff.readUInt8(offset);

		//Player ID =  r,g,b
		out.playerID = out.r + ',' + out.g + ',' + out.b;

		//Position
		offset += 1;
		out.x  = buff.readFloatLE(offset);
		
		offset += 4;
		out.y  = buff.readFloatLE(offset);
	}
	//Not a Packet, forward as string
	else{
		out = null;
	}

	return out;

}


var clients= {
	tcp: 0,
	udp: 0,
	browser: 0
}

//Sends new client stats to browser
var SendClientUpdate = function(){
	io.emit('clients', clients);
}

//Sends Packet to browsers
var SendPacket = function(socketType, buffer){
	io.emit('message', {type: socketType, message: buffer});
}

//Web Socket Server
var io = require('socket.io')(websocketPort);

io.on('connection', function (socket) {
	clients.browser++;
  console.log('Connection from WebSocket');
  
  //Update Client Stats
  SendClientUpdate();

  //io.emit('this', { will: 'be received by everyone'});

  socket.on('message', function(msg){
  	console.log('I received a message from ', socket.id, ' saying ', msg);
  	//socket.emit('message', 'Server Says Hello');
  });

  socket.on('disconnect', function () {
  	clients.browser--;
    console.log('User Disconnected from Web');
    SendClientUpdate();
  });
});

//TCP Server
var net = require('net');
var tcpServer = net.createServer(function(socket){
	

	//socket.on('connect', function(){
		//Identify Client
		var addy = socket.address();
		socket.name = addy.address + ":" + addy.port;
		console.log("Got TCP connection from: " + socket.name);

		clients.tcp++;
		SendClientUpdate();
	//});

	socket.on('data', function(data){
		SendPacket('TCP', data);
		console.log('Got TCP Message: ' + (data + ''));
		console.log('From : ' + socket.name);

		//echo
		socket.write(data);
	});

	socket.on('end', function(){
		
        console.log("End Connection from: " + socket.name);
    });
    
    socket.on('close', function(){
    	clients.tcp--;
		SendClientUpdate();
        console.log("Closed Connection from: " + socket.name);
    });
    
    socket.on('error', function(){
        console.log("Error on Socket from: " + socket.name);
    });
});
tcpServer.listen(tcpPort);

//UDP Server
var dgram = require('dgram');
var udpClients = tools.CMap();
var numUDPClients = 0;
var UpdateUDPClients = function(){
	var time = Date.now();
	var disconnected  = [];
	for(var i = udpClients.size; i > 0; i--){
		if((time - udpClients.value()) > 5000){
			disconnected.push( udpClients.key());
		} 
		udpClients.next();
	}

	if(disconnected.length > 0){
		for(var j = disconnected.length - 1; j >= 0; j--){
			udpClients.remove(disconnected[j]);
		}
		clients.udp = udpClients.size;
		numUDPClients = udpClients.size;
		SendClientUpdate();
	}
	else{
		if((numUDPClients != udpClients.size)){
			clients.udp = udpClients.size;
			numUDPClients = udpClients.size;
			SendClientUpdate();
		}
	}
	

	setTimeout(UpdateUDPClients, 5000);
}
var udpServer = dgram.createSocket('udp4');
udpServer.bind(udpPort);
udpServer.on('message', function(msg, sender){
	//Parse Packet
	var obj = PacketParse(msg);

	if(obj){
		//Packet was decoded
		SendPacket('UDP', obj);
		console.log('Packet ID: ' + obj.id);

	}
	else{
		//No Packet Data, forward binary
		SendPacket('UDP', msg);
		console.log('Got UDP Message: ' + (msg + ''));

		//Echo back to sender
		udpServer.send(msg, 0, msg.length, sender.port, sender.address);
	}

	udpClients.put(sender.address, Date.now());
	console.log('From IP: ' + sender.address + ':' + sender.port);
});

setTimeout(UpdateUDPClients, 5000);

console.log("Web Server Listening on Port: " + websocketPort);
console.log("TCP Server Listening on Port: " + tcpPort);
console.log("UDP Server Listening on Port: " + udpPort);


